import React from 'react';
import { Link, Route, Switch } from 'react-router-dom';
import { Empty } from './Empty';
import { Words } from './Words';
import { Questions } from './Questions';
import { Admin } from './Admin';
import './App.scss';

export const App: React.FC = () => (
    <div className="App">
        <nav className="navbar bg-light navbar-expand-sm navbar-light">
            <ul className="navbar-nav mr-auto">
                <li className="navbar-item">
                    <Link to="/words" className="nav-link">
                        Words
                    </Link>
                </li>
                <li className="navbar-item">
                    <Link to="/questions" className="nav-link">
                        Questions
                    </Link>
                </li>
            </ul>
        </nav>
        <Switch>
            <Route exact path="/" component={Empty} />
            <Route path="/words" component={Words} />
            <Route path="/questions" component={Questions} />
            <Route path="/admin" component={Admin} />
        </Switch>
    </div>
);
