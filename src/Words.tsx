import React, { useCallback, useEffect, useState } from 'react';
import { Word } from './Word';
import { Progress } from './Progress';
import { Result } from './Result';
import { WrongWords } from './WrongWords';
import { useFetchWords } from './Firebase';
import admin from 'firebase';

type DocumentData = admin.firestore.DocumentData;

export const Words: React.FC = () => {
    const [words, setWords] = useState<DocumentData[]>([]);
    const [index, setIndex] = useState(0);
    const [results, setResults] = useState<number[]>(Array(words.length).fill(0));
    const [done, setDone] = useState(false);
    const [wrongs, setWrongs] = useState<DocumentData[]>([]);

    const shuffleArray = useCallback((array: DocumentData[]) => {
        for (let i = array.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]];
        }
        setWords(array);
    }, []);

    useFetchWords(shuffleArray);

    useEffect(() => {
        setResults(Array(words.length).fill(0));
    }, [words]);

    const nextWord = useCallback(
        (correct: boolean) => {
            results[index] = correct ? 2 : 1;
            setResults(results);
            if (!correct) {
                setWrongs([...wrongs, words[index]]);
            }
            if (index === results.length - 1) {
                setDone(true);
            } else {
                setIndex((index) => index + 1);
            }
        },
        [results, index, wrongs, words]
    );

    const restart = useCallback(() => {
        setDone(false);
        setResults(Array(words.length).fill(0));
        setIndex(0);
        setWrongs([]);
    }, [words]);

    if (!words) return null;

    return (
        <div>
            {done ? (
                <Result results={results} restart={restart} />
            ) : (
                <Word data={words[index]} next={nextWord} restart={restart} counter={index + '/' + words.length} />
            )}
            <Progress results={results} />
            <WrongWords wrongs={wrongs} />
        </div>
    );
};
