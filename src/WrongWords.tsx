import React from 'react';
import admin from 'firebase';
import { WrongWord } from './WrongWord';

type DocumentData = admin.firestore.DocumentData;

type WrongProps = {
    wrongs: DocumentData[];
};

export const WrongWords: React.FC<WrongProps> = ({ wrongs }) => {
    if (wrongs.length === 0) return null;

    return (
        <div className="wrong">
            You got {wrongs.length} wrong:
            {wrongs.map((w, i) => (
                <WrongWord word={w} key={i} />
            ))}
        </div>
    );
};
