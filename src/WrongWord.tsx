import React from 'react';
import admin from 'firebase';
import { FaExchangeAlt } from 'react-icons/fa';

type DocumentData = admin.firestore.DocumentData;

export const WrongWord: React.FC<DocumentData> = (word) => (
    <div>
        <span className="list">{word.word.de.join(' | ')}</span>
        <FaExchangeAlt className="btn-large" />
        <span className="list">{word.word.en.join(' | ')}</span>
    </div>
);
