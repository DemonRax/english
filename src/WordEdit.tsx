import React, { ChangeEvent, MouseEvent, useCallback, useState } from 'react';
import { Button, Col, Dropdown, DropdownButton, Form, InputGroup, Row } from 'react-bootstrap';

type WordEditProps = {
    isNew: boolean;
    de?: string[];
    en?: string[];
    type?: string;
    save: (de: string[], en: string[], type: string) => string | null;
};

const cap = (s?: string) => {
    if (!s) return 'Word';
    return s.charAt(0).toUpperCase() + s.slice(1);
};

const initArray = (a?: string[]) => Object.assign(Array<string>(5).fill(''), a || []);

export const WordEdit: React.FC<WordEditProps> = ({ isNew, de, en, type, save }) => {
    const [renderDe, setRenderDe] = useState<string[]>(() => initArray(de));
    const [renderEn, setRenderEn] = useState<string[]>(() => initArray(en));
    const [renderType, setRenderType] = useState(cap(type));

    const onTypeChange = useCallback(
        (type: string) => (e: MouseEvent) => {
            setRenderType(cap(type));
        },
        []
    );

    const onChange = useCallback(
        (lang: string, i: number) => (e: ChangeEvent<HTMLInputElement>) => {
            setRenderDe(
                renderDe.map((v, j) => {
                    if (lang === 'de' && i === j) {
                        return e.target.value;
                    }
                    return v;
                })
            );
            setRenderEn(
                renderEn.map((v, j) => {
                    if (lang === 'en' && i === j) {
                        return e.target.value;
                    }
                    return v;
                })
            );
        },
        [renderDe, renderEn, setRenderDe, setRenderEn]
    );

    const reset = useCallback(() => {
        setRenderDe(initArray(de));
        setRenderEn(initArray(en));
        setRenderType(cap(type));
    }, [de, en, type]);

    const onSave = useCallback(
        (_: MouseEvent) => {
            save(renderDe, renderEn, renderType.toLowerCase());
            reset();
        },
        [reset, renderDe, renderEn, renderType, save]
    );

    const onAdd = useCallback(
        (_: MouseEvent) => {
            save(renderDe, renderEn, renderType.toLowerCase());
            reset();
        },
        [renderDe, renderEn, renderType, reset, save]
    );

    const onReset = useCallback(
        (_: MouseEvent) => {
            reset();
        },
        [reset]
    );

    return (
        <Form>
            <Form.Group as={Row}>
                <Col xs={1} className="col">
                    <Form.Label>DE</Form.Label>
                </Col>
                {[0, 1, 2, 3, 4].map((i) => (
                    <Col xs={2} key={i} className="col">
                        <Form.Control type="text" value={renderDe[i]} onChange={onChange('de', i)} />
                    </Col>
                ))}
                <Col xs={1} className="col">
                    <InputGroup className="mb-3">
                        <DropdownButton variant="outline-secondary" title={renderType} id="input-group-dropdown-1">
                            <Dropdown.Item onClick={onTypeChange('Word')}>Word</Dropdown.Item>
                            <Dropdown.Item onClick={onTypeChange('Number')}>Number</Dropdown.Item>
                        </DropdownButton>
                    </InputGroup>
                </Col>
            </Form.Group>
            <Form.Group as={Row}>
                <Col xs={1} className="col">
                    <Form.Label>EN</Form.Label>
                </Col>
                {[0, 1, 2, 3, 4].map((i) => (
                    <Col xs={2} key={i} className="col">
                        <Form.Control type="text" value={renderEn[i]} onChange={onChange('en', i)} />
                    </Col>
                ))}
                <Col xs={1} className="col">
                    Level
                </Col>
            </Form.Group>
            <Form.Group as={Row}>
                <Col xs={12}>
                    {isNew ? (
                        <div>
                            <Button variant="primary" onClick={onAdd}>
                                Add
                            </Button>
                            <Button variant="secondary" onClick={onReset}>
                                Reset
                            </Button>
                        </div>
                    ) : (
                        <div>
                            <Button variant="success" onClick={onSave}>
                                Save
                            </Button>
                            <Button variant="secondary" onClick={onReset}>
                                Reset
                            </Button>
                        </div>
                    )}
                </Col>
            </Form.Group>
        </Form>
    );
};
