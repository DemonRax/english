import React from 'react';

type ResultProps = {
    results: number[];
    restart: () => void;
};

const score = (results: number[]): number => {
    let sum = 0;
    results.forEach((r) => {
        sum += r === 2 ? 1 : 0;
    });
    return sum / results.length;
};

const msg = (score: number): string => {
    const percent = Math.round(score * 100) + '% ';
    let msg = 'Very bad, please try again...';
    if (score >= 0.5) msg = 'Not bad, but can be much better...';
    if (score >= 0.9) msg = 'Very good! Most of them are correct!';
    if (score === 1) msg = 'Dude perfect! Great job!';
    return percent + msg;
};

const style = (score: number): string => {
    let s = 'fail';
    if (score >= 0.5) s = 'good';
    if (score > 0.9) s = 'great';
    if (score === 1) s = 'perfect';
    return 'result result--' + s;
};

export const Result: React.FC<ResultProps> = ({ results, restart }) => (
    <div className="result field">
        <div className={style(score(results))}>{msg(score(results))}</div>
        <div className="guess">
            <button onClick={restart}>Restart</button>
        </div>
    </div>
);
