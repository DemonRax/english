import React from 'react';

type ProgressProps = {
    results: number[];
};

const style = (r: number): string => {
    let s = 'null';
    if (r === 1) s = 'false';
    if (r === 2) s = 'true';
    return 'bar__child bar__child--' + s;
};

export const Progress: React.FC<ProgressProps> = ({ results }) => (
    <div className="bar">
        {results.map((r, i) => (
            <div className={style(r)} key={i} />
        ))}
    </div>
);
