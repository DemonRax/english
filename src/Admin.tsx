import React, { ChangeEvent, useCallback, useEffect, useState } from 'react';
import { Accordion, Card } from 'react-bootstrap';
import { FaRegEdit, FaRegTrashAlt, FaPlusCircle, FaMinusCircle, FaExchangeAlt, FaCode } from 'react-icons/fa';
import { WordEdit } from './WordEdit';
import { addWord, deleteWord, useFetchWords } from './Firebase';
import admin from 'firebase';

type DocumentData = admin.firestore.DocumentData;

const contains = (a: string[], s: string) => {
    s = s.toLowerCase();
    let found = false;
    a.forEach((w: string) => {
        if (w.toLowerCase().includes(s)) found = true;
    });
    return found;
};

export const Admin: React.FC = () => {
    const [words, setWords] = useState<DocumentData[]>([]);
    const [show, setShow] = useState<DocumentData[]>([]);
    const [eventKey, setEventKey] = useState('');

    useFetchWords(setWords);

    useEffect(() => {
        setShow(words);
    }, [words]);

    const onEdit = useCallback(() => {
        // console.log('edit');
    }, []);

    const onDelete = useCallback(
        (id: string) => () => {
            deleteWord(id);
        },
        []
    );

    const onSave = useCallback((de: string[], en: string[], type: string): string | null => addWord(de, en, type), []);

    const openNew = useCallback(() => {
        setEventKey(eventKey === '' ? 'open' : '');
    }, [eventKey]);

    const onSearchChange = useCallback(
        (e: ChangeEvent<HTMLInputElement>) => {
            const search = e.target.value;
            if (search === '') setShow(words);
            setShow(words.filter((v) => contains(v.de, search) || contains(v.en, search)));
        },
        [words]
    );

    return (
        <div className="admin">
            <div>
                <span>Total words in the database: {words.length}</span>
                <span className="btn-large btn-middle" title="Add new">
                    {eventKey === 'open' ? <FaMinusCircle onClick={openNew} /> : <FaPlusCircle onClick={openNew} />}
                </span>
            </div>
            <Accordion activeKey={eventKey}>
                <Card key="open" className="card-no-border">
                    <Accordion.Collapse eventKey="open">
                        <Card.Body>
                            <WordEdit isNew={true} save={onSave} />
                        </Card.Body>
                    </Accordion.Collapse>
                </Card>
            </Accordion>
            <div>
                Search: <input type="text" onChange={onSearchChange} />
                <span className="found">Found: {show.length}</span>
            </div>
            {show.map((v, i) => (
                <div key={i}>
                    <span className="btn-large" title="Edit">
                        <FaRegEdit onClick={onEdit} />
                    </span>
                    <span className="btn-large" title="Delete">
                        <FaRegTrashAlt onClick={onDelete(v.id)} />
                    </span>
                    <span className="list">{v.de.join(' | ')}</span>
                    <FaExchangeAlt className="btn-large" />
                    <span className="list">{v.en.join(' | ')}</span>
                    <FaCode className="btn-large" />
                    <span className="list">{v.type}</span>
                </div>
            ))}
        </div>
    );
};
