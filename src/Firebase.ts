import { useEffect } from 'react';
import { app } from './Base';
import admin from 'firebase';

type DocumentData = admin.firestore.DocumentData;
const dbWords = app.firestore().collection('dict');
// const dbQuestions = app.firestore().collection('questions');

const compact = (a: string[]): string[] => a.filter((v) => !!v);

export const useFetchWords = (setWords: (d: DocumentData[]) => void): void => {
    useEffect(
        () =>
            dbWords.onSnapshot((snapshot) => {
                const temp: DocumentData[] = [];
                snapshot.forEach((doc) => {
                    temp.push({ ...doc.data(), id: doc.id });
                });
                setWords(temp);
            }),
        [setWords]
    );
};

export const addWord = (de: string[], en: string[], type: string): string | null => {
    de = compact(de);
    en = compact(en);
    if (de.length === 0 || en.length === 0) {
        return 'error';
    }
    dbWords.add({
        de: de,
        en: en,
        type: type ? type : 'Word'
    });
    return null;
};

export const deleteWord = (id: string): void => {
    dbWords.doc(id).delete();
};
