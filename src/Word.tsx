import React, { ChangeEvent, useCallback, useEffect, useState, KeyboardEvent } from 'react';
import admin from 'firebase';

type DocumentData = admin.firestore.DocumentData;

const random = (array: any) => array[Math.floor(Math.random() * array.length)];

const includes = (array: string[], guess: string): boolean => {
    guess = guess.toLowerCase();
    let found = false;
    array.forEach((word: string) => {
        if (word.toLowerCase() === guess) {
            found = true;
        }
    });
    return found;
};

type WordProps = {
    data: DocumentData | null;
    next: (correct: boolean) => void;
    restart: () => void;
    counter: string;
};

export const Word: React.FC<WordProps> = ({ data, next, restart, counter }) => {
    const [word, setWord] = useState('');
    const [guess, setGuess] = useState('');

    useEffect(() => {
        if (data) {
            setWord(random(data.de));
            setGuess('');
        }
    }, [data]);

    const onChange = useCallback((e: ChangeEvent<HTMLInputElement>) => {
        setGuess(e.target.value);
    }, []);

    const checkGuess = useCallback(() => {
        if (!data || !guess) {
            return;
        }
        next(includes(data.en, guess));
    }, [data, guess, next]);

    const onEnter = useCallback(
        (e: KeyboardEvent<HTMLInputElement>) => {
            e.preventDefault();
            if (e.key === 'Enter') {
                checkGuess();
            }
        },
        [checkGuess]
    );

    if (!data) return null;

    return (
        <div className="field">
            <div className="word">{word}</div>
            <div className="guess">
                <input
                    autoFocus
                    type="text"
                    placeholder="English"
                    value={guess}
                    onChange={onChange}
                    onKeyUp={onEnter}
                />
                <button onClick={checkGuess}>Next</button>
                <button onClick={restart}>Restart</button>
                <span className="guess">{counter}</span>
            </div>
        </div>
    );
};
